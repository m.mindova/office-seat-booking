$(function() {
    $(document).on("click", ".btn-add-seats", addSeats);
});

function addSeats() {
    let numSeats = $('.num-seats').val();

    if (numSeats && numSeats > 0) {
        for ($i = 0; $i < numSeats; $i++) {
            $('.seats-container').append('<input type="text" name="OfficeForm[seats][]" id="seat-' + $i + '"  class="form-control"></input><br>');
        }
    }
}