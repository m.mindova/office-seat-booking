$(function() {
    $(document).on("click", ".btn-book-seat", bookSeat);
});

function bookSeat() {
    let form = $(this).closest("form");
    let url = form.attr("action");
    let btnBookSeat = ".btn-book-seat";

    $(btnBookSeat).attr("disabled", true);

    $.post(url, form.serialize(), function(response) {
        if (response.html) {
            form.html(response.html);
        }
        $(btnBookSeat).attr("disabled", false);
    });
}