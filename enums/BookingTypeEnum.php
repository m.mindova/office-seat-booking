<?php 

namespace app\enums;

use yii\base\BaseObject;

class BookingTypeEnum extends BaseObject
{
    const TYPE_HOUR = 1;
    const TYPE_DAY  = 2;
}