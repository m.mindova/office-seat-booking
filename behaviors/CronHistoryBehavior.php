<?php

namespace app\behaviors;

use yii\base\Behavior;
use yii\console\Controller;
use app\models\CronHistory;

/**
 * This behavior inserts a new record in the cron_history table
 * after a cron action is exectuted
 */
class CronHistoryBehavior extends Behavior
{
    public function events()
    {
        return array_merge(parent::events(), [
            Controller::EVENT_AFTER_ACTION => 'recordCronHistory',
        ]);
    }

    public function recordCronHistory()
    {
        $cron = new CronHistory();
        $cron->cron_action = $this->owner->action->id;
        $cron->save();
    }
}