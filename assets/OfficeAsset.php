<?php

namespace app\assets;

use yii\web\AssetBundle;

class OfficeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $js = [
        'js/office.js'
    ];
    public $depends = [
        'app\assets\AppAsset',
    ];
}
