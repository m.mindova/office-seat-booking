<?php

namespace app\assets;

use yii\web\AssetBundle;

class BookSeatAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $js = [
        'js/book-seat.js'
    ];
    public $depends = [
        'app\assets\AppAsset',
    ];
}
