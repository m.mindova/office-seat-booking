<?php

namespace app\forms;

use Yii;
use yii\helpers\VarDumper;
use yii\base\Model;
use app\models\Office;
use app\models\Seat;

/**
 * @property Office $office
 * 
 * OfficeForm is the model behind the office form.
 */
class OfficeForm extends Model
{
    /** @var string */
    public $name;

    /** @var array */
    public $seats;

    /** @var Office */
    private $_office;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    /** 
     * @param Office $office
     * @param array $config
     */
    public function __construct(Office $office, array $config = [])
    {
        $this->_office = $office;

        if (!$office->isNewRecord) {
            $this->name = $office->name;
        }
        parent::__construct($config);
    }

    /**
     * @return Office
     */
    public function getOffice(): Office
    {
        return $this->_office;
    }

    /**
     * @return array the validation rules.
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['name'], 'filter', 'filter' => 'trim'],
            [['seats'], 'safe'],
        ];
    }

     /**
     * @return array
     */
    public function scenarios(): array
    {
        $scenarios = parent::scenarios();

        $scenarios[static::SCENARIO_CREATE] = ['name', 'seats'];
        $scenarios[static::SCENARIO_UPDATE] = ['name', 'seats'];

        return $scenarios;
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels(): array
    {
        return [
            'name' => 'Name',
        ];
    }

    /**
     * @return bool Whether saving the office record was successful
     */
    public function saveOffice(): bool
    {
        $this->office->name = $this->name;

        if (!$this->office->save()) {
            Yii::error(VarDumper::dumpAsString($this->office->getErrors()));

            return false;
        }
        $this->saveSeats();

        return true;
    }

    /**
     * Insert or update Seat records, related to the Office record,
     * according to scenario
     */
    private function saveSeats()
    {
        if ($this->scenario === self::SCENARIO_CREATE) {
            $this->insertSeats();
        }
        if ($this->scenario === self::SCENARIO_UPDATE) {
            $this->updateSeats();
        }
    }

    /**
     * Insert Seat records for this office 
     */
    private function insertSeats()
    {
        foreach ($this->seats as $seatName) {
            if (!$this->office->getSeats()->where(['name' => $seatName, 'office_id' => $this->office->id])->exists()) {
                $seat = new Seat();
                $seat->name = $seatName;
                $seat->office_id = $this->office->id;

                if (!$seat->save()) {
                    Yii::error(VarDumper::dumpAsString($seat->getErrors()));
                }
            }
        }
    }

    /**
     * Update Seat record of this Office
     */
    private function updateSeats()
    {
        foreach ($this->seats as $seatId => $seatName) {
            $seat = Seat::findOne($seatId);
            if ($seat) {
                $seat->name = $seatName;
                $seat->save();
            }
        }
    }
}
