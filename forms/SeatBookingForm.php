<?php

namespace app\forms;

use app\businesslayer\EmployeeSeatFactory;
use yii\base\Model;
use app\businesslayer\SeatBL;
use app\models\Seat;
use app\enums\BookingTypeEnum;
use DateTime;

/**
 * @property Seat $seat
 * @property array $hours
 * 
 * BookSeatForm is the model behind the seat booking form.
 */
class SeatBookingForm extends Model
{
    /** @var integer */
    public $employeeId;

    /** @var array */
    public $selectedHours;

    /** @var array */
    private $_hours;

    /** @var boolean */
    public $workDay;

    /** @var Seat */
    private $_seat;

    /** 
     * @param Employee $employee
     * @param array $config
     */
    public function __construct(Seat $seat, array $config = [])
    {
        $this->_seat = $seat;

        parent::__construct($config);
    }

    /**
     * @return Seat
     */
    public function getSeat(): Seat
    {
        return $this->_seat;
    }

    /**
     * @return array the validation rules.
     */
    public function rules(): array
    {
        return [
            [['workDay'], 'boolean'],
            [['employeeId'], 'required'],
            [['selectedHours'], 'required', 'when' => function (self $model) {
                return $model->workDay == false;
            }],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels(): array
    {
        return [
            'employeeId'     => 'Employee',
            'selectedHours'  => 'Hours',
        ];
    }

    /**
     * Populate the hours (free and occupied) of this seat
     */
    private function setHours()
    {
        $hours = ['08', '09', '10', '11', '12', '13', '14', '15', '16', '17'];

        if ($this->seat->getEmployeeSeats()->where(['booking_type_id' => BookingTypeEnum::TYPE_DAY])->exists()) {
            $hoursArray = array_fill_keys($hours, 'taken');
        } else {
            $hoursArray = array_fill_keys($hours, 'free');

            foreach ($this->seat->employeeSeats as $employeeSeat) {
                $occupiedHour = (new \DateTime($employeeSeat->start_datetime))->format('H');

                if (in_array($occupiedHour, $hours)) {
                    $hoursArray[$occupiedHour] = 'taken';
                }
            }
        }
        $this->_hours = $hoursArray;
    }

    /**
     * @return array
     */
    public function getHours()
    {
        if (!$this->_hours) {
            $this->setHours();
        }
        return $this->_hours;
    }

    /**
     * Book the seat for the selected hours or for a whole day
     */
    public function bookSeat()
    {
        if ($this->workDay) {
            return $this->bookForWorkDay();
        }
        return $this->bookForHour();
    }

    /**
     * Book the seat for the whole working day
     */
    private function bookForWorkDay()
    {
        $dateTime = (new DateTime())->setTime(8, 0)->format(DATE_ATOM);
        $factory = new EmployeeSeatFactory();

        return $factory->create($this->employeeId, $this->_seat->id, $dateTime, BookingTypeEnum::TYPE_DAY);
    }

    /**
     * Book the seat for the selected hours
     */
    private function bookForHour()
    {
        $success = true;
        foreach ($this->selectedHours as $hour) {
            $dateTime = (new DateTime())->setTime((int)$hour, 0)->format(DATE_ATOM);
            $factory = new EmployeeSeatFactory();

            $success = $factory->create($this->employeeId, $this->_seat->id, $dateTime, BookingTypeEnum::TYPE_HOUR) && $success;
        }
        return $success;
    }
}
