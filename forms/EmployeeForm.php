<?php

namespace app\forms;

use Yii;
use yii\helpers\VarDumper;
use yii\base\Model;
use app\models\Employee;

/**
 * @property Employee $employee
 * 
 * EmployeeForm is the model behind the employee form.
 */
class EmployeeForm extends Model
{
    /** @var string */
    public $first_name;

    /** @var string */
    public $last_name;

    /** @var string */
    public $payroll_number;

    /** @var string */
    public $email;

    /** @var Employee */
    private $_employee;

    /** 
     * @param Employee $employee
     * @param array $config
     */
    public function __construct(Employee $employee, array $config = [])
    {
        $this->_employee = $employee;

        if (!$employee->isNewRecord) {
            $this->first_name     = $employee->first_name;
            $this->last_name      = $employee->last_name;
            $this->payroll_number = $employee->payroll_number;
            $this->email          = $employee->email;
        }

        parent::__construct($config);
    }

    /**
     * @return Employee
     */
    public function getEmployee(): Employee
    {
        return $this->_employee;
    }

    /**
     * @return array the validation rules.
     */
    public function rules(): array
    {
        return [
            [['first_name', 'last_name', 'payroll_number', 'email'], 'required'],
            [['first_name', 'last_name', 'payroll_number', 'email'], 'filter', 'filter' => 'trim'],
            [['payroll_number'], 'filter', 'filter' => 'strtoupper'],
            ['email', 'email'],
            ['payroll_number', 'string', 'length' => 10]
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels(): array
    {
        return [
            'first_name'     => 'First Name',
            'last_name'      => 'Last Name',
            'payroll_number' => 'Payroll Number',
            'email'          => 'Email',
        ];
    }

    /**
     * @return bool Whether saving the employee record was successful
     */
    public function saveEmployee(): bool
    {
        $this->employee->setAttributes([
            'first_name'     => $this->first_name,
            'last_name'      => $this->last_name,
            'payroll_number' => $this->payroll_number,
            'email'          => $this->email,
        ]);
        if (!$this->employee->save()) {
            Yii::error(VarDumper::dumpAsString($this->employee->getErrors()));

            return false;
        }
        return true;
    }
}
