<?php

use yii\db\Migration;

/**
 * Class m211114_074503_create_table_office
 */
class m211114_074503_create_table_office extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('office', [
            'id'   => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('office');
    }
}
