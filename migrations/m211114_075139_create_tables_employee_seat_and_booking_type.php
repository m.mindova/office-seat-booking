<?php

use yii\db\Migration;

/**
 * Class m211114_075139_create_tables_employee_seat_and_booking_type
 */
class m211114_075139_create_tables_employee_seat_and_booking_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //Create table booking_type and insert records

        $this->createTable('booking_type', [
            'id'   => $this->primaryKey(),
            'name' => $this->string(50),
        ]);

        $this->batchInsert('booking_type', ['id', 'name'], [
            [1, '1 Hour'],
            [2, '1 Working day'],
        ]);


        //Create table employee_seat

        $this->createTable('employee_seat', [
            'employee_id'     => $this->integer(),
            'seat_id'         => $this->integer(),
            'start_datetime'  => $this->timestamp()->append('default now()'),
            'booking_type_id' => $this->integer()->defaultValue(1),
            'PRIMARY KEY(employee_id, seat_id, start_datetime)',
        ]);

        $this->createIndex('idx_employee_seat_employee', 'employee_seat', 'employee_id');
        $this->addForeignKey('fk_employee_seat_employee', 'employee_seat', 'employee_id', 'employee', 'id');

        $this->createIndex('idx_employee_seat_seat', 'employee_seat', 'seat_id');
        $this->addForeignKey('fk_employee_seat_seat', 'employee_seat', 'seat_id', 'seat', 'id');

        $this->createIndex('idx_employee_seat_booking_type', 'employee_seat', 'booking_type_id');
        $this->addForeignKey('fk_employee_seat_booking_type', 'employee_seat', 'booking_type_id', 'booking_type', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('employee_seat');
        $this->dropTable('booking_type');
    }
}
