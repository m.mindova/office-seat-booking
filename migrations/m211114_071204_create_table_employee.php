<?php

use yii\db\Migration;

/**
 * Class m211114_071204_create_table_employee
 */
class m211114_071204_create_table_employee extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('employee', [
            'id'             => $this->primaryKey(),
            'first_name'     => $this->string(50)->notNull(),
            'last_name'      => $this->string(50)->notNull(),
            'payroll_number' => $this->string(10)->unique(),
            'email'          => $this->string(60)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('employee');
    }
}
