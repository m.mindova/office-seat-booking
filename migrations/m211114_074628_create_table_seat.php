<?php

use yii\db\Migration;

/**
 * Class m211114_074628_create_table_seat
 */
class m211114_074628_create_table_seat extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('seat', [
            'id'        => $this->primaryKey(),
            'name'      => $this->string(50)->notNull(),
            'office_id' => $this->integer()
        ]);
        
        $this->createIndex('idx_seat_office', 'seat', 'office_id');
        $this->addForeignKey('fk_seat_office', 'seat', 'office_id', 'office', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('seat');
    }
}
