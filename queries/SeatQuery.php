<?php

namespace app\queries;

/**
 * This is the ActiveQuery class for [[\app\models\Seat]].
 *
 * @see \app\models\Seat
 */
class SeatQuery extends \yii\db\ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \app\models\Seat[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Seat|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    // /**
    //  * @return ActiveQuery
    //  */
    // public function available()
    // {
    //     return $this->andWhere('select not exists (select 1 from employee_seat where employee_seat.seat_id = seat.id)');
    // }

    //     /**
    //  * @return ActiveQuery
    //  */
    // public function occupied()
    // {
    //     return $this->andWhere('select exists (select 1 from employee_seat where employee_seat.seat_id = seat.id)');
    // }
}
