<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Office seat booking system</h1>

        <p><?= Html::a('Book a seat', Url::to(['booking/index']), ['class' => 'btn btn-lg btn-success']); ?> </p>

    </div>

</div>
