<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Office;
use yii\grid\ActionColumn;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Offices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="office-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'name',
            [
                'class'     => ActionColumn::class,
                'template'  => '{book}',
                'buttons'   => [
                    'book' => function (string $url, Office $model) {
                        $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-calendar"]);

                        return Html::a($icon, Url::to(['booking/office-seats', 'id' => $model->id]), [
                            'title' => 'Book a seat',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>