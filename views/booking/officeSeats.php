<?php

use yii\helpers\Html;
use app\assets\BookSeatAsset;
use app\forms\SeatBookingForm;

/* @var $this yii\web\View */
/* @var $office app\businesslayer\OfficeBL */

BookSeatAsset::register($this);

$this->title = "{$office->name} seats";
$this->params['breadcrumbs'][] = ['label' => 'Offices', 'url' => ['booking/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="office-seats">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php foreach ($office->seats as $seat) : ?>
        <div class="col-xs-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <strong>Seat <?= $seat->name ?></strong>
                </div>

                <?= $this->render('_hours', ['model' => new SeatBookingForm($seat)]) ?>

            </div>
        </div>
    <?php endforeach; ?>
</div>