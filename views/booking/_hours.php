<?php

use yii\helpers\Html;
use app\businesslayer\EmployeeBL;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $seat app\businesslayer\SeatBL */
/* @var $model app\forms\SeatBookingForm */

$labelOptionsAvailable = ['class' => 'btn btn-info'];
$labelOptionsTaken = ['class' => 'btn btn-warning disabled'];

if (\Yii::$app->session->hasFlash('error_booking')) { ?>
    <div class="alert alert-warning">
        <?= Yii::$app->session->getFlash('error_booking'); ?>
    </div>
<?php }

$form = ActiveForm::begin([
    'id'      => "form-seat-{$model->seat->id}",
    'action'  => Url::to(['book-seat', 'id' => $model->seat->id]),
]);
?>
<div class="panel-body">
    <div class="row form-group">
        <?php
        echo $form->field($model, 'employeeId')->dropDownList(ArrayHelper::map(EmployeeBL::find()->all(), 'id', 'fullName'), [
            'prompt' => 'Select Employee',
            'class'  => 'employee-dropdown form-control'
        ])->label(false);

        echo $form->field($model, 'selectedHours', [
            'options' => ['class' => 'hours-checkboxlist']
        ])->checkboxList($model->hours, ['item' => function ($index, $label, $name, $checked, $value) use ($labelOptionsTaken, $labelOptionsAvailable) {
            $taken = $label === 'taken';
            return Html::checkbox($name, $checked, [
                'value'        => $value,
                'disabled'     => $taken ? true : false,
                'label'        => $value,
                'labelOptions' => $taken ? $labelOptionsTaken : $labelOptionsAvailable,
            ]);
        }])->label(false);

        echo $form->field($model, 'workDay')->checkbox([
            'class'        => 'work-day-checkbox',
            'label'        => 'All Day',
            'disabled'     => in_array('taken', $model->hours) ? true : false,
            'labelOptions' => in_array('taken', $model->hours) ? $labelOptionsTaken : $labelOptionsAvailable
        ])->label(false);
        ?>
    </div>
    <div class="row">
        <?= Html::button('Book', ['class' => 'btn-book-seat btn btn-primary']); ?>
    </div>
</div>
<?php
ActiveForm::end();
