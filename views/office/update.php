<?php

use app\assets\OfficeAsset;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Office */

OfficeAsset::register($this);

$this->title = 'Update Office: ' . $model->office->name;
$this->params['breadcrumbs'][] = ['label' => 'Offices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->office->name, 'url' => ['view', 'id' => $model->office->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="office-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
