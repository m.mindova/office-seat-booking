<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Office;

/* @var $this yii\web\View */
/* @var $model app\models\Office */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Offices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="office-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'label' => 'Seats',
                'value' => function (Office $office) {
                    if (!$office->seats) {
                        return null;
                    }
                    return implode(', ', $office->getSeats()->select('name')->column());
                }
            ]
        ],
    ]) ?>

</div>