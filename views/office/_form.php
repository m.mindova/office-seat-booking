<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\forms\OfficeForm;

/* @var $this yii\web\View */
/* @var $model app\forms\OfficeForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="office-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php if ($model->scenario === OfficeForm::SCENARIO_CREATE) {
        echo Html::label('Number of seats', 'num_seats');
        echo Html::tag('br');
        echo Html::textInput('num_seats', '', ['type' => 'number', 'class' => 'num-seats form-group']);
        echo Html::button('Add seats', ['class' => 'btn-add-seats btn btn-info']);
    } ?>

    <div class="seats-container">
        <?php
        if ($model->scenario === OfficeForm::SCENARIO_UPDATE) {
            echo Html::label('Seats', 'OfficeForm[seats]');
        }
        foreach ($model->office->seats as $seat) {
            echo Html::textInput("OfficeForm[seats][{$seat->id}]", $seat->name, ['id' => "seat-{$seat->id}", 'class' => 'form-control']);
            // echo Html::textInput("OfficeForm[seats][]", $seat->name, ['id' => "seat-{$seat->id}", 'class' => 'form-control']);
            echo Html::tag('br');
        }
        ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>