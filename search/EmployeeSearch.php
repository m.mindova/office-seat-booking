<?php

namespace app\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\businesslayer\EmployeeBL;

/**
 * EmployeeSearch represents the model behind the search form of `app\models\Employee`.
 */
class EmployeeSearch extends EmployeeBL
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payroll_number'], 'integer'],
            [['first_name', 'last_name', 'email'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmployeeBL::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->emulateExecution();
        }

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'payroll_number', $this->payroll_number]);

        return $dataProvider;
    }
}
