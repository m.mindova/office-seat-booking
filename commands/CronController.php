<?php

namespace app\commands;

use yii\console\Controller;
use app\behaviors\CronHistoryBehavior;
use app\businesslayer\SeatAvailabilityHelper;
use app\models\EmployeeSeat;

/**
 * This controller is used for executing cron commands.
 */
class CronController extends Controller
{
    /**
     * @return array
     */
    public function behaviors(): array
    {
        return array_merge(parent::behaviors(), [
            CronHistoryBehavior::class,
        ]);
    }

    /**
     * Check occupied seats and update their availability if needed
     */
    public function actionUpdateSeatAvailability()
    {
        $occupiedSeats = EmployeeSeat::find()->all();

        $helper = new SeatAvailabilityHelper($occupiedSeats);
        $helper->checkAvailability();
    }

}
