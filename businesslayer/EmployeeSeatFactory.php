<?php

namespace app\businesslayer;

use app\models\EmployeeSeat;
use Yii;
use yii\base\BaseObject;
use yii\helpers\VarDumper;

/**
 * This is the factory class for model EmployeeSeat
 */
class EmployeeSeatFactory extends BaseObject
{
    /**
     * @param integer $employeeId
     * @param integer $seatId
     * @param string $dateTime
     * @param integer $bookingTypeId
     * 
     * @return boolean True if saving the record was successful; false - if not
     */
    public function create(int $employeeId, int $seatId, string $dateTime, int $bookingTypeId)
    {
        $employeeSeat = new EmployeeSeat();
        $employeeSeat->setAttributes([
            'employee_id'     => $employeeId,
            'seat_id'         => $seatId,
            'start_datetime'  => $dateTime,
            'booking_type_id' => $bookingTypeId
        ]);
        if (!$employeeSeat->save()) {
            Yii::error(VarDumper::dumpAsString($employeeSeat->getErrors()));

            return false;
        }
        return true;
    }
}