<?php

namespace app\businesslayer;

use app\enums\BookingTypeEnum;
use app\models\EmployeeSeat;
use DateInterval;
use DateTime;
use yii\base\BaseObject;

class SeatAvailabilityHelper extends BaseObject
{

    /** @var EmployeeSeat[] */
    private $_employeeSeats;

    public function __construct(array $employeeSeats, array $config = [])
    {
        $this->_employeeSeats = $employeeSeats;

        parent::__construct($config);
    }

    /**
     * Iterate over all occupied seats and updates their availability if needed
     */
    public function checkAvailability()
    {
        $todayDateTime = (new DateTime());
        $todayDate = $todayDateTime->format('d/m/Y');
        $currentTime = $todayDateTime->format('H:i');

        foreach ($this->_employeeSeats as $employeeSeat) {
            $startDateTime = (new DateTime($employeeSeat->start_datetime));

            if ($startDateTime->format('d/m/Y') < $todayDate) {
                $this->emptySeat($employeeSeat);
                continue;
            }

            if ($startDateTime->format('d/m/Y') === $todayDate) {
                if ($employeeSeat->booking_type_id === BookingTypeEnum::TYPE_DAY) {
                    continue;
                } else {
                    if ($startDateTime->add(new DateInterval('PT1H'))->format('H:i') <= $currentTime) {
                        $this->emptySeat($employeeSeat);
                    }
                }
            }
        }
    }

    /**
     * Empty seat for the occupied hour
     * @param EmployeeSeat $employeeSeat
     */
    private function emptySeat(EmployeeSeat $employeeSeat)
    {
        $employeeSeat->delete();
    }
}
