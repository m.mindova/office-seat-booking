<?php

namespace app\businesslayer;

use app\models\Employee;

/**
 * @property string fullName
 * @property string fullNamePayroll
 * 
 * Contains most frequentyly used businesslayer logic for Employee model
 */
class EmployeeBL extends Employee
{
    
     /**
     * @return string Employee first and  last name
     */
    public function getFullName(): string
    {
        return "{$this->first_name} {$this->last_name}";
    }


    /**
     * @return string Employee first, last name and payroll number
     */
    public function getFullNamePayroll(): string
    {
        return  "{$this->fullName} {$this->payroll_number}";
    }
}
