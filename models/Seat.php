<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "seat".
 *
 * @property int $id
 * @property string $name
 * @property int|null $office_id
 *
 * @property EmployeeSeat[] $employeeSeats
 * @property Employee[] $employees
 * @property Office $office
 */
class Seat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'seat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['office_id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['office_id'], 'exist', 'skipOnError' => true, 'targetClass' => Office::class, 'targetAttribute' => ['office_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'name'      => 'Name',
            'office_id' => 'Office ID',
        ];
    }

    /**
     * Gets query for [[EmployeeSeats]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeSeats()
    {
        return $this->hasMany(EmployeeSeat::class, ['seat_id' => 'id']);
    }

    /**
     * Gets query for [[Employees]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployees()
    {
        return $this->hasMany(Employee::class, ['id' => 'employee_id'])->viaTable('employee_seat', ['seat_id' => 'id']);
    }

    /**
     * Gets query for [[Office]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOffice()
    {
        return $this->hasOne(Office::class, ['id' => 'office_id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\queries\SeatQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\queries\SeatQuery(get_called_class());
    }
}
