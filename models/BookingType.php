<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "booking_type".
 *
 * @property int $id
 * @property string|null $name
 *
 * @property EmployeeSeat[] $employeeSeats
 */
class BookingType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'booking_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'   => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[EmployeeSeats]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeSeats()
    {
        return $this->hasMany(EmployeeSeat::class, ['booking_type_id' => 'id']);
    }
}
