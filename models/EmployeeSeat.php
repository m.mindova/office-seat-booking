<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee_seat".
 *
 * @property int $employee_id
 * @property int $seat_id
 * @property string $start_datetime
 * @property int|null $booking_type_id
 *
 * @property BookingType $bookingType
 * @property Employee $employee
 * @property Seat $seat
 */
class EmployeeSeat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee_seat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employee_id', 'seat_id', 'start_datetime'], 'required'],
            [['employee_id', 'seat_id', 'booking_type_id'], 'integer'],
            [['start_datetime'], 'safe'],
            [['employee_id', 'seat_id', 'start_datetime'], 'unique', 'targetAttribute' => ['employee_id', 'seat_id', 'start_datetime']],
            [['booking_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingType::class, 'targetAttribute' => ['booking_type_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['employee_id' => 'id']],
            [['seat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Seat::class, 'targetAttribute' => ['seat_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'employee_id'     => 'Employee ID',
            'seat_id'         => 'Seat ID',
            'start_datetime'  => 'Start Datetime',
            'booking_type_id' => 'Booking Type ID',
        ];
    }

    /**
     * Gets query for [[BookingType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBookingType()
    {
        return $this->hasOne(BookingType::class, ['id' => 'booking_type_id']);
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::class, ['id' => 'employee_id']);
    }

    /**
     * Gets query for [[Seat]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSeat()
    {
        return $this->hasOne(Seat::class, ['id' => 'seat_id']);
    }
}
