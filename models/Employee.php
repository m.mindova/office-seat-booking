<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string|null $payroll_number
 * @property string $email
 *
 * @property EmployeeSeat[] $employeeSeats
 * @property Seat[] $seats
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email'], 'required'],
            [['first_name', 'last_name'], 'string', 'max' => 50],
            [['payroll_number'], 'string', 'max' => 10],
            [['email'], 'string', 'max' => 60],
            [['payroll_number'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'first_name'     => 'First Name',
            'last_name'      => 'Last Name',
            'payroll_number' => 'Payroll Number',
            'email'          => 'Email',
        ];
    }

    /**
     * Gets query for [[EmployeeSeats]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeSeats()
    {
        return $this->hasMany(EmployeeSeat::class, ['employee_id' => 'id']);
    }

    /**
     * Gets query for [[Seats]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSeats()
    {
        return $this->hasMany(Seat::class, ['id' => 'seat_id'])->viaTable('employee_seat', ['employee_id' => 'id']);
    }
}
