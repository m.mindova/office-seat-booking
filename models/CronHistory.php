<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cron_history".
 *
 * @property string $cron_action
 * @property string $datetime
 */
class CronHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cron_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cron_action'], 'required'],
            [['datetime'], 'safe'],
            [['cron_action'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cron_action' => 'Cron Action',
            'datetime' => 'Datetime',
        ];
    }
}
