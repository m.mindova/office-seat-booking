<?php

namespace app\controllers;

use Yii;
use app\models\Office;
use app\search\OfficeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\Seat;
use app\forms\SeatBookingForm;
use yii\web\Response;

class BookingController extends Controller
{
    /**
     * Lists all Office models.
     * @return mixed
     */
    public function actionIndex(): string
    {
        $searchModel = new OfficeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @param integer $id Office ID
     */
    public function actionOfficeSeats(int $id): string
    {
        $office = $this->findModel($id);

        return $this->render('officeSeats', compact('office'));
    }

    /**
     * @param integer $id Seat ID
     * 
     * @return yii\web\Response
     */
    public function actionBookSeat(int $id): Response
    {
        $seat = Seat::findOne($id);
        if (!$seat) {
            throw new NotFoundHttpException('The requested record does not exist.');
        }
        $model = new SeatBookingForm($seat);

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $success = $model->bookSeat();

            if (!$success) {
                Yii::$app->session->setFlash('error_booking', "There was an error trying to book one or more of the selected hours.");
            }
        }
        return $this->asJson([
            'html' => $this->renderPartial('_hours', compact('model'))
        ]);
    }

    /**
     * Finds the Office model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Office the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id): Office
    {
        if (($model = Office::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
